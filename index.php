<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Display information about all the contentsimulator modules in the requested course.
 *
 * @package     contentsimulator
 * @copyright   2019 Your Name <you@example.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require(__DIR__.'/../../config.php');
require_once(__DIR__.'/mod_form.php');
require_once(__DIR__.'/lib.php');
require_once(__DIR__.'/launchserver.php');

$id = required_param('id', PARAM_INT);

$course = $DB->get_record('course', array('id' => $id), '*', MUST_EXIST);
require_course_login($course);

$coursecontext = context_course::instance($course->id);

$event = \mod_contentsimulator\event\course_module_instance_list_viewed::create(array(
    'context' => $modulecontext
));
$event->add_record_snapshot('course', $course);
$event->trigger();

$PAGE->set_url('/mod/contentsimulator/index.php', array('id' => $id));
$PAGE->set_title(format_string($course->fullname));
$PAGE->set_heading(format_string($course->fullname));
$PAGE->set_context($coursecontext);


echo $OUTPUT->header();

$modulenameplural = get_string('modulenameplural', 'contentsimulator');
echo $OUTPUT->heading($modulenameplural);

require_once(__DIR__.'/mod_form.php');

$ti_form = new mod_contentsimulator_mod_form(__DIR__.'/mod_form.php');    // how to receive data in this new page
     if ($ti_form->is_cancelled()) {
        echo '<h1>not received</h1>';
        // Redirect to somewhere if the user clicks cancel
    }

    if ($data = $ti_form->get_data()) {
        echo '<h1>data received</h1>';
        // Do something with the data, then redirect to a new page
    } 

    // You should really output the page header before this
    $ti_form->display();


$contentsimulators = get_all_instances_in_course('contentsimulator', $course);

if (empty($contentsimulators)) {
    notice(get_string('nonewmodules', 'contentsimulator'), new moodle_url('/course/view.php', array('id' => $course->id)));
}

$table = new html_table();
$table->attributes['class'] = 'generaltable mod_index';

if ($course->format == 'weeks') {
    $table->head  = array(get_string('week'), get_string('name'));
    $table->align = array('center', 'left');
} else if ($course->format == 'topics') {
    $table->head  = array(get_string('topic'), get_string('name'));
    $table->align = array('center', 'left', 'left', 'left');
} else {
    $table->head  = array(get_string('name'));
    $table->align = array('left', 'left', 'left');
}

foreach ($contentsimulators as $contentsimulator) {
    if (!$contentsimulator->visible) {
        $link = html_writer::link(
            new moodle_url('/mod/contentsimulator/view.php', array('id' => $contentsimulator->coursemodule)),
            format_string($contentsimulator->name, true),
            array('class' => 'dimmed'));
    } else {
        $link = html_writer::link(
            new moodle_url('/mod/contentsimulator/view.php', array('id' => $contentsimulator->coursemodule)),
            format_string($contentsimulator->name, true));
    }

    if ($course->format == 'weeks' or $course->format == 'topics') {
        $table->data[] = array($contentsimulator->section, $link);
    } else {
        $table->data[] = array($link);
    }
}



echo html_writer::table($table);

echo $OUTPUT->footer();
